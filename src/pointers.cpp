#include <iostream>

using namespace std;


int main()
{
	int x = 25, y = 2 * x;    
	auto a = &x, b = a;
	auto c = *a, d = 2 * *b;    
    
	cout<<"x value= "<< x <<" and adress = "<< &x <<endl; // x is a int
	cout<<"y value= "<< y <<" and adress = "<< &y <<endl; // y is a int
	cout<<"a value= "<< *a <<" and adress = "<< a <<endl; // a is the location of the variable x
	cout<<"b value= "<< *b <<" and adress = "<< b <<endl; // b is the same as a so it is the location of the variable x
	cout<<"c value= "<< c <<" and adress = "<< &c <<endl; // c is pointing to the variable inside the location a so it is the same as the int x
	cout<<"d value= "<< d <<" and adress = "<< &d <<endl; // d is pointing to the variable inside a which is the same as a which is the int x (25) multiplied by 2
 



}
